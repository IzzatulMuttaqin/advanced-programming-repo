import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!

    private Map<Integer, List<String>> groupScore;
    private Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

        groupScore = new HashMap<>();

        groupScore.put(11, Arrays.asList("Charlie", "Foxtrot"));
        groupScore.put(12, Arrays.asList("Alice"));
        groupScore.put(15, Arrays.asList("Emi", "Bob", "Delta"));
    }

    @Test
    public void testGroupByScores() {
        assertNotNull(ScoreGrouping.groupByScores(scores));
        assertEquals(groupScore, ScoreGrouping.groupByScores(scores));
    }

}