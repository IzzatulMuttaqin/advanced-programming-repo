package applicant;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.function.Predicate;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!
    private Applicant applicant;

    @Before
    public void setUp() {
        applicant = new Applicant();
    }

    @Test
    public void methodAndMethodEvaluatortest() {
        Assert.assertTrue(Applicant.evaluateMethod(applicant,
                new CreditEvaluator(new QualifiedEvaluator())));
        Assert.assertTrue(Applicant.evaluateMethod(applicant,
                new CreditEvaluator(new EmploymentEvaluator(new QualifiedEvaluator()))));
        Assert.assertFalse(Applicant.evaluateMethod(applicant,
                new CriminalRecordsEvaluator(
                        new EmploymentEvaluator(new QualifiedEvaluator()))));
        Assert.assertFalse(Applicant.evaluateMethod(applicant,
                new CriminalRecordsEvaluator(
                        new CreditEvaluator(
                                new EmploymentEvaluator(new QualifiedEvaluator())))));
    }

    @Test
    public void evaluateTestAndPrintEvaluate() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();

        System.setOut(new PrintStream(outContent));

        Predicate<Applicant> qualifiedEvaluator = Applicant::isCredible;
        Predicate<Applicant> creditEvaluator = applicant1 -> applicant1.getCreditScore() > 600;
        Predicate<Applicant> employmentEvaluator =
            applicant1 -> applicant1.getEmploymentYears() > 0;
        Predicate<Applicant> checkCrime = applicant1 -> !applicant1.hasCriminalRecord();

        Assert.assertTrue(Applicant.evaluate(applicant,
                creditEvaluator.and(qualifiedEvaluator)));
        Assert.assertTrue(Applicant.evaluate(applicant,
                employmentEvaluator.and(qualifiedEvaluator)));
        Assert.assertFalse(Applicant.evaluate(applicant,
                qualifiedEvaluator.and(employmentEvaluator).and(checkCrime)));
        Assert.assertFalse(Applicant.evaluate(applicant,
                qualifiedEvaluator.and(employmentEvaluator)
                        .and(creditEvaluator).and(checkCrime)));

        Applicant.printEvaluation(
                Applicant.evaluate(applicant, creditEvaluator.and(qualifiedEvaluator)));
        Applicant.printEvaluation(
                Applicant.evaluate(applicant,
                        qualifiedEvaluator.and(employmentEvaluator)
                                .and(creditEvaluator).and(checkCrime)));
        Assert.assertTrue(outContent.toString()
                .contains("Result of evaluating applicant: accepted"));
        Assert.assertTrue(outContent.toString()
                .contains("Result of evaluating applicant: rejected"));
    }
}
