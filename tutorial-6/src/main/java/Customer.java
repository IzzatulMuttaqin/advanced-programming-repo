import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" +
                    String.valueOf(each.getThisAmount()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints())
                + " frequent renter points";

        return result;
    }

    public String htmlStatement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "<br>";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "&nbsp;&nbsp;&nbsp;&nbsp;" + each.getMovie().getTitle()
                    + "&nbsp;&nbsp;&nbsp;&nbsp;"
                    + String.valueOf(each.getThisAmount()) + "<br>";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "<br>";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints())
                + " frequent renter points";

        return result;
    }

    private double getTotalAmount() {
        double totalAmount = 0;

        for (Rental rental : rentals) {
            totalAmount += rental.getThisAmount();
        }

        return totalAmount;
    }

    private int getTotalFrequentRenterPoints(){
        int result = 0;

        for (Rental rental : rentals) {
            result += rental.getFrequentRenterPoints();
        }

        return result;
    }

}