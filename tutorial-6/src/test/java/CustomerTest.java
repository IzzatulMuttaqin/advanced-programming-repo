import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Customer customer;
    private Movie movie1;
    private Movie movie2;
    private Movie movie3;
    private Rental rental1;
    private Rental rental2;
    private Rental rental3;

    @Before
    public void setUp() {
        this.customer = new Customer("Alice");

        this.movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        this.rental1 = new Rental(this.movie1, 3);

        this.movie2 = new Movie("Bad Black", Movie.NEW_RELEASE);
        this.rental2 = new Rental(this.movie2, 2);

        this.movie3 = new Movie("Bad Black", Movie.CHILDREN);
        this.rental3 = new Rental(this.movie3, 4);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        this.customer.addRental(rental1);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        this.customer.addRental(rental1);
        this.customer.addRental(rental2);
        this.customer.addRental(rental3);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        System.out.println(result);
        assertTrue(result.contains("Amount owed is 12.5"));
        assertTrue(result.contains("4 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rental1);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5<br>"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rental1);
        customer.addRental(rental2);
        customer.addRental(rental3);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 12.5<br>"));
        assertTrue(result.contains("4 frequent renter points"));
    }

}