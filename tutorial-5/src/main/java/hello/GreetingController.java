package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                       String name, Model model) {
        model.addAttribute("name", name);
        if (name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", name + ", I hope you are interested to hire me");
        }

        StringBuilder curriculumVitae = new StringBuilder();
        curriculumVitae.append("Name: IzzatulMuttaqin\n");
        curriculumVitae.append("Birthdate: 20/04/1997\n");
        curriculumVitae.append("Birthplace: Balikpapan\n");
        curriculumVitae.append("Address: Jl. Banjar RT 7 No 8 Balikpapan\n");
        curriculumVitae.append("Education History:\n");
        curriculumVitae.append("- SMP N 1 Balikpapan 2009-2012\n");
        curriculumVitae.append("- SMA N 1 Balikpapan 2013-2016\n");
        curriculumVitae.append("- Faculty of Computer Science 2016-Present\n");
        model.addAttribute("curriculumVitae", curriculumVitae.toString());

        StringBuilder description = new StringBuilder();
        description.append("I am a second-year CS student at Universitas Indonesia. ");
        description.append("Recently, interviewed with GDP Labs. ");
        description.append("Currently, I'm teaching assistant and Kakak Asuh at UI. ");
        description.append("Perhaps. ");
        model.addAttribute("description", description.toString());

        return "greeting";

    }

}
