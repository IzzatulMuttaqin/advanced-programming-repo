package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class BrieCheese implements Cheese {
    public String toString() {
        return "Melted Brie";
    }
}
