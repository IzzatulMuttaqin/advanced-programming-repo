package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ShreddedCrustDough implements Dough {

    public String toString() {
        return "Shredded Crust Dough";
    }
}
