package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Excercise1Test {

	private Cheese mozarella, parmesan, reggiano;
    private Clams fresh, frozen;
    private Dough thick, thin;
    private Sauce marinara, plumTomato;

    @Before
    public void setUp() {
        //Cheese
        mozarella = new MozzarellaCheese();
        parmesan = new ParmesanCheese();
        reggiano = new ReggianoCheese();


        //Clam
        fresh = new FreshClams();
        frozen = new FrozenClams();


        //Dough
        thick = new ThickCrustDough();
        thin = new ThinCrustDough();


        //Sauce
        plumTomato = new PlumTomatoSauce();
        marinara = new MarinaraSauce();

    }

    @Test
    public void testToString() {
        //CheeseString
        assertEquals("Shredded Mozzarella", mozarella.toString());
        assertEquals("Shredded Parmesan", parmesan.toString());
        assertEquals("Reggiano Cheese", reggiano.toString());


        //Clam
        assertEquals("Fresh Clams from Long Island Sound", fresh.toString());
        assertEquals("Frozen Clams from Chesapeake Bay", frozen.toString());


        //Dough
        assertEquals("ThickCrust style extra thick crust dough", thick.toString());
        assertEquals("Thin Crust Dough", thin.toString());


        //Sauce
        assertEquals("Tomato sauce with plum tomatoes", plumTomato.toString());
        assertEquals("Marinara Sauce", marinara.toString());

    }

	@Test
    public void testCanCreatePizza() {
        PizzaStore depokPizzaStore = new DepokPizzaStore();

        Pizza dpPizza = depokPizzaStore.orderPizza("cheese");
        assertNotNull(dpPizza);

        PizzaStore newYorkPizzaStore = new NewYorkPizzaStore();

        Pizza nyPizza = newYorkPizzaStore.orderPizza("cheese");
        assertNotNull(nyPizza);
        nyPizza = newYorkPizzaStore.orderPizza("clam");
        assertNotNull(nyPizza);
        nyPizza = newYorkPizzaStore.orderPizza("veggie");
        assertNotNull(nyPizza);
    }
}
